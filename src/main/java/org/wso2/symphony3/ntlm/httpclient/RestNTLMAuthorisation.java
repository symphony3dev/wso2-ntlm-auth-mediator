package org.wso2.symphony3.ntlm.httpclient;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMNode;
import org.apache.axiom.om.OMText;
import org.apache.axiom.soap.SOAPBody;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.EntityEnclosingMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseConstants;
import org.apache.synapse.SynapseException;
import org.apache.synapse.mediators.AbstractMediator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class RestNTLMAuthorisation extends AbstractMediator {

	// Logging
	private static final Log log = LogFactory.getLog(RestNTLMAuthorisation.class);

	// The kind of HTTP Method to use
	// One of: GET, POST, PUT, or DELETE
	private String httpMethodPropertyName;

	// The property name where your endpoint to point the REST client at is stored
	// (INCLUDING protocol and port)... Example: http://localhost:9000
	private String httpEndpointPropertyName;

	// If you're working with POST or PUT, you may have a request body to submit
	// This is the property name where your request body string will be stored
	private String httpRequestBodyPropertyName;

	// The property name where the NTLM authentication domain is stored
	private String authDomainPropertyName;

	// The property name where where your NTLM authentication host name is stored (e.g. localhost)
	private String authHostPropertyName;

	// The property name where your NTLM username is stored
	private String authUsernamePropertyName;

	// The property name where your NTLM password is stored
	private String authPasswordPropertyName; //Password must be retrieved from synapse message context (might be encrypted)

	// can add more methods here, but this is what we'll support for now
	private static enum HTTP_METHOD { GET, POST, PUT, DELETE }

	public boolean mediate(MessageContext context) {

		HTTP_METHOD httpMethod = HTTP_METHOD.valueOf(context.getProperty(httpMethodPropertyName).toString());
		String httpEndpoint = context.getProperty(httpEndpointPropertyName).toString();
		String authHost = context.getProperty(authHostPropertyName).toString();
		String authDomain = context.getProperty(authDomainPropertyName).toString();
		String authUsername = context.getProperty(authUsernamePropertyName).toString();
		String authPassword = context.getProperty(authPasswordPropertyName).toString();

		// optional configuration
		String httpRequestBody = "";
		if( context.getProperty(httpRequestBodyPropertyName) != null) {
			httpRequestBody = context.getProperty(httpRequestBodyPropertyName).toString();
		}

		// Build NTLM Authentication Scheme
		AuthPolicy.registerAuthScheme(AuthPolicy.NTLM, JCIFS_NTLMScheme.class);

		// Assign authentication scheme to http client
		HttpClient client = new HttpClient();
		List authPrefs = new ArrayList(1);
		authPrefs.add(AuthPolicy.NTLM);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Add authentication details to HTTP Client via CredentialsProvider
		NTCredentials creds = new NTCredentials(authUsername, authPassword, authHost, authDomain);
		client.getState().setCredentials(AuthScope.ANY, creds);

		// Force Authentication - failures will get caught in the catch block
		try {

			// Build method dynamically
			HttpMethod method;
			switch(httpMethod) {

				case GET:
					method = new GetMethod(httpEndpoint);
					break;

				case POST:
					method = new PostMethod(httpEndpoint);

					break;

				case PUT:
					method = new PutMethod(httpEndpoint);
					break;

				case DELETE:
					method = new DeleteMethod(httpEndpoint);
					break;

				default:
					method = new GetMethod(httpEndpoint);
					break;

			}

			// Attach some data to the request body if provided
			if(httpRequestBody != null || httpRequestBody != "") {
				StringRequestEntity strRequestEntity = new StringRequestEntity(httpRequestBody, null, null);
				((EntityEnclosingMethod) method).setRequestEntity(strRequestEntity);
			}

			// log out some details about the request for debugging
			Object[] params = new Object[]{method.toString(), httpEndpoint, httpRequestBody};
			log.info(MessageFormat.format("RestNTLMAuth: Sending request. Method: {0}, Endpoint: {1}, Request Body {2}", params));

			// execute this
			int resultCode = client.executeMethod(method);

			// You can use this to determine whether the request was unsuccessful or not
			context.setProperty("REST_NTLM_AUTH_RESPONSE_CODE", resultCode);

			final String strGetResponseBody = method.getResponseBodyAsString();

			// important to release the connection after use to keep things flowing
			method.releaseConnection();

			// Retrieve Result and replace message context
			SOAPEnvelope e = context.getEnvelope();
			if (e != null) {
				SOAPBody b = e.getBody();
				Iterator children = b.getChildren();
				while (children.hasNext()) {
					Object o = children.next();
					if (o instanceof OMNode) {
						children.remove();
					}
				}
				OMText elem = OMAbstractFactory.getOMFactory().createOMText(strGetResponseBody);
				b.addChild(elem);
			}

		} catch (Exception e) {

			context.setProperty(SynapseConstants.ERROR_EXCEPTION, e);
			context.setProperty(SynapseConstants.ERROR_MESSAGE,  e.getMessage());
			context.setProperty(SynapseConstants.ERROR_CODE, "S3_NTLM_AUTH_ERR");

			throw new SynapseException(e);

		}

		return true;

	}

	public void setHttpMethodPropertyName(String _httpMethodPropertyName){
		httpMethodPropertyName = _httpMethodPropertyName;
	}

	public String getHttpMethodPropertyName(){
		return httpMethodPropertyName;
	}

	public void setHttpEndpointPropertyName(String _httpEndpointPropertyName){
		httpEndpointPropertyName = _httpEndpointPropertyName;
	}

	public String getHttpEndpointPropertyName(){
		return httpEndpointPropertyName;
	}

	public void setHttpRequestBodyPropertyName(String _httpRequestBodyPropertyName){
		httpRequestBodyPropertyName = _httpRequestBodyPropertyName;
	}

	public String getHttpRequestBodyPropertyName(){
		return httpRequestBodyPropertyName;
	}

	public void setAuthDomainPropertyName(String _authDomainPropertyName){
		authDomainPropertyName = _authDomainPropertyName;
	}

	public String getAuthDomainPropertyName(){
		return authDomainPropertyName;
	}

	public void setAuthHostPropertyName(String _authHostPropertyName){
		authHostPropertyName = _authHostPropertyName;
	}

	public String getAuthHostPropertyName(){
		return authHostPropertyName;
	}

	public void setAuthUsernamePropertyName(String _authUsernamePropertyName){
		authUsernamePropertyName = _authUsernamePropertyName;
	}

	public String getAuthUsernamePropertyName(){
		return authUsernamePropertyName;
	}

	public void setAuthPasswordPropertyName(String _authPasswordPropertyName){
		authPasswordPropertyName = _authPasswordPropertyName;
	}

	public String getAuthPasswordPropertyName(){
		return authPasswordPropertyName;
	}

}
