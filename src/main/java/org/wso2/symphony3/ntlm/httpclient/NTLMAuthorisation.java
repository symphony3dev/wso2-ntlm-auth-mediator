package org.wso2.symphony3.ntlm.httpclient;

import java.util.ArrayList;
import java.util.List;

import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.OperationClient;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.axis2.wsdl.WSDLConstants;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.synapse.MessageContext; 
import org.apache.synapse.SynapseConstants;
import org.apache.synapse.SynapseException;
import org.apache.synapse.mediators.AbstractMediator;

public class NTLMAuthorisation extends AbstractMediator { 

	private String soapActionPropertyName;
	private String soapEndpointPropertyName;
	private String domainPropertyName;
	private String hostPropertyName;
	private String portPropertyName;
	private String usernamePropertyName;
	private String passwordPropertyName; //Password must be retrieved from synapse message context (might be encrypted)
	
	public boolean mediate(MessageContext context) { 
		
		String soapAction = context.getProperty(soapActionPropertyName).toString();
		String soapEndpoint = context.getProperty(soapEndpointPropertyName).toString();
		String domain = context.getProperty(domainPropertyName).toString();
		String host = context.getProperty(hostPropertyName).toString();
		Integer port = Integer.parseInt(context.getProperty(portPropertyName).toString());
		String username = context.getProperty(usernamePropertyName).toString();
		String password = context.getProperty(passwordPropertyName).toString();
		
		//Build NTLM Authentication Scheme
		AuthPolicy.registerAuthScheme(AuthPolicy.NTLM, JCIFS_NTLMScheme.class);
		HttpTransportProperties.Authenticator auth = new HttpTransportProperties.Authenticator();
		auth.setUsername( username );
		auth.setPassword( password );
		auth.setDomain( domain );
		auth.setHost( host );
		auth.setPort( port );
		ArrayList<String> authPrefs = new ArrayList<String>();
		authPrefs.add(AuthPolicy.NTLM);
		auth.setAuthSchemes(authPrefs);
		
		//Force Authentication - failures will get caught in the catch block
		try {
			
			//Build ServiceClient and set Authorization Options
			ServiceClient serviceClient = new ServiceClient();
			Options options = new Options();
			options.setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
			options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
			options.setTo(new EndpointReference(soapEndpoint));
			options.setAction(soapAction);
			serviceClient.setOptions(options);
			
			//Generate an OperationClient from the ServiceClient to execute the request
			OperationClient opClient = serviceClient.createClient(ServiceClient.ANON_OUT_IN_OP);
			
			//Have to translate MsgCtx from Synapse to Axis2
			org.apache.axis2.context.MessageContext axisMsgCtx = new org.apache.axis2.context.MessageContext();  
			axisMsgCtx.setEnvelope(context.getEnvelope());
			opClient.addMessageContext(axisMsgCtx);
			
			//Send the request to the server
			opClient.execute(true);
			
			//Retrieve Result and replace mediation (synapse) context
			SOAPEnvelope result = opClient.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE).getEnvelope();
			context.setEnvelope(result);
			
		} catch (Exception e) {
			
			context.setProperty(SynapseConstants.ERROR_EXCEPTION, e);
			context.setProperty(SynapseConstants.ERROR_MESSAGE,  e.getMessage());
			context.setProperty(SynapseConstants.ERROR_CODE, "S3_NTLM_AUTH_ERR");
			
			throw new SynapseException(e);
			
		}
		
		return true;
	
	}

	public void setSoapActionPropertyName(String _soapActionPropertyName){
		soapActionPropertyName = _soapActionPropertyName;
	}
	
	public String getSoapActionPropertyName(){
		return soapActionPropertyName;
	}
	
	public void setSoapEndpointPropertyName(String _soapEndpointPropertyName){
		soapEndpointPropertyName = _soapEndpointPropertyName;
	}
	
	public String getSoapEndpointPropertyName(){
		return soapEndpointPropertyName;
	}
	
	public void setDomainPropertyName(String _domainPropertyName){
		domainPropertyName = _domainPropertyName;
	}
	
	public String getDomainPropertyName(){
		return domainPropertyName;
	}
	
	public void setHostPropertyName(String _hostPropertyName){
		hostPropertyName = _hostPropertyName;
	}
	
	public String getHostPropertyName(){
		return hostPropertyName;
	}
	
	public void setPortPropertyName(String _portPropertyName){
		portPropertyName = _portPropertyName;
	}
	
	public String getPortPropertyName(){
		return portPropertyName;
	}
	
	public void setUsernamePropertyName(String _usernamePropertyName){
		usernamePropertyName = _usernamePropertyName;
	}
	
	public String getUsernamePropertyName(){
		return usernamePropertyName;
	}
	
	public void setPasswordPropertyName(String _passwordPropertyName){
		passwordPropertyName = _passwordPropertyName;
	}
	
	public String getPasswordPropertyName(){
		return passwordPropertyName;
	}
	
}
