# WSO2 NTLM Authorisation Mediator
The reason for creating a custom mediator is because NTLM authentication (which is required to proceed through various Microsoft-based corporate proxies) are unsupported in WSO2.

## Installation
1. Stop WSO2 ESB
2. Locate the JAR file (see TECHNICAL-DOC.md for details on how to build from source). Pre-packaging JAR file is available in BitBucket under "Downloads".
3. Transfer it to the [ESB_HOME]/repository/components/dropins directory.
4. Start the WSO2 ESB for it to detect the new external library.

## Usage
In your sequence, you should now be able to add a class mediator and reference the custom mediator class in the JAR file using the package name and class name together, for example: **org.wso2.symphony3.ntlm.httpclient.NTLMAuthorisation**.

There are two types of custom mediator classes we'll touch on in this section: `NTLMAuthorisation` and `RestNTLMAuthorisation`.

### NTLM Authorisation

**Class Name:** `org.wso2.symphony3.ntlm.httpclient.NTLMAuthorisation`

Use this class in the event that you require NTLM Authorisation for working with a SOAP-based API. There are a number of properties that must be provided to the class mediator in order for this to function correctly. Properties must be defined in the mediation flow and their names passed into the class mediator for reference:

  - `soapActionPropertyName`: The SOAP action you would like to invoke on the server.

  - `soapEndpointPropertyName`: The SOAP endpoint to hit (i.e. some IP address/domain).

  - `domainPropertyName`: The authentication domain.

  - `hostPropertyName`: The authentication host (should be similar to endpoint).

  - `portPropertyName`: The endpoint's port.

  - `usernamePropertyName`: The authentication user.

  - `passwordPropertyName`: The authentication password.

When the class mediator executes, it will attempt to retrieve a response payload and replace the sequences SOAP envelope with the one returned from the server.

### REST NTLM Authorisation

**Class Name:** `org.wso2.symphony3.ntlm.httpclient.RestNTLMAuthorisation`

Similar to the NTLM Authorisation class, the RestNTLMAuthorisation class is also used to authenticate with NTLM-based corporate proxies. However, the core difference here is that it's designed to handle RESTful API requests, rather than SOAP ones. Again, there are a number of properties that must be defined in the sequence prior to calling the class mediator. These property names must be referenced in the class mediator configuration:

  - `httpMethodPropertyName`: The kind of HTTP Method to use. One of: `GET`, `POST`, `PUT`, or `DELETE`.

  - `httpEndpointPropertyName`: The property name where your endpoint to point the REST client at is stored (INCLUDING protocol and port). Example: http://localhost:9000.

  - `httpRequestBodyPropertyName`: If you're working with POST or PUT, you may have a request body to submit. This is the property name where your request body string will be stored. This field is optional, so you can leave it blank if unnecessary.

  - `authDomainPropertyName`: The property name where the NTLM authentication domain is stored.

  - `authHostPropertyName`: The property name where where your NTLM authentication host name is stored (e.g. localhost).

  - `authUsernamePropertyName`: The property name where your NTLM username is stored.

  - `authPasswordPropertyName`: The property name where your NTLM password is stored.

When the mediator executes, the response body and HTTP status code is captured. The response body is dumped as text into the SOAP body of the sequence. The HTTP status code is captured in the `REST_NTLM_AUTH_RESPONSE_CODE` property and should be used to decide whether or not a successful request was rendered.

#### Full REST NTLM Example Sequence

```
<?xml version="1.0" encoding="UTF-8"?>
<sequence name="rest_auth_test_ntlm2" trace="enable" xmlns="http://ws.apache.org/ns/synapse">
    <log level="custom">
        <property name="message" value="Test NTLM for REST API"/>
    </log>
    <payloadFactory media-type="xml">
        <format>
            <request xmlns="">
                <entity>Phone</entity>
                <action>create</action>
                <key>7a211f7f46367a1427655e16a48f5376</key>
                <user>Api</user>
                <pass>sym3pw</pass>
                <api_key>1ekh1ayra2322bwqskl</api_key>
                <version>3</version>
                <phone>02323</phone>
                <contact_id>4</contact_id>
            </request>
        </format>
    </payloadFactory>
    <property name="FORCE_HTTP_CONTENT_LENGTH" scope="axis2"
        type="STRING" value="true"/>
    <property name="COPY_CONTENT_LENGTH_FROM_INCOMING" scope="axis2"
        type="STRING" value="true"/>
    <property name="messageType" scope="axis2" type="STRING" value="text/xml"/>
    <property name="ContentType" scope="axis2" type="STRING" value="text/xml"/>
    <property action="remove" name="NO_ENTITY_BODY" scope="axis2"/>
    <property name="DISABLE_CHUNKING" scope="axis2" type="STRING" value="true"/>
    <property name="OUT_ONLY" scope="default" type="STRING" value="true"/>
    <enrich>
        <source clone="false" type="body"/>
        <target action="replace" property="httpRequestBody" type="property"/>
    </enrich>
    <property name="httpMethod" scope="default" type="STRING" value="POST"/>
    <property name="httpEndpoint" scope="default" type="STRING" value="http://52.62.140.34:83/sites/all/modules/civicrm/extern/rest.php?entity=Phone&amp;action=create&amp;key=7a211f7f46367a1427655e16a48f5376&amp;user=Api&amp;pass=sym3pw&amp;api_key=1ekh1ayra2322bwqskl&amp;version=3&amp;phone=111111&amp;contact_id=1"/>
    <property name="authDomain" scope="default" type="STRING" value="WIN-5T3LP55GFVL"/>
    <property name="authHost" scope="default" type="STRING" value="52.62.140.34"/>
    <property name="authUsername" scope="default" type="STRING" value="Administrator"/>
    <property name="authPassword" scope="default" type="STRING" value="wDgDBSchY*f"/>
    <header action="remove" name="To" scope="default"/>
    <log level="full"/>
    <class name="org.wso2.symphony3.ntlm.httpclient.RestNTLMAuthorisation">
        <axis2ns141:property name="authDomainPropertyName"
            value="authDomain" xmlns:axis2ns141="http://ws.apache.org/ns/synapse"/>
        <axis2ns142:property name="httpEndpointPropertyName"
            value="httpEndpoint" xmlns:axis2ns142="http://ws.apache.org/ns/synapse"/>
        <axis2ns143:property name="httpMethodPropertyName"
            value="httpMethod" xmlns:axis2ns143="http://ws.apache.org/ns/synapse"/>
        <axis2ns144:property name="authUsernamePropertyName"
            value="authUsername" xmlns:axis2ns144="http://ws.apache.org/ns/synapse"/>
        <axis2ns145:property name="authHostPropertyName"
            value="authHost" xmlns:axis2ns145="http://ws.apache.org/ns/synapse"/>
        <axis2ns146:property name="httpRequestBodyPropertyName"
            value="httpRequestBody" xmlns:axis2ns146="http://ws.apache.org/ns/synapse"/>
        <axis2ns147:property name="authPasswordPropertyName"
            value="authPassword" xmlns:axis2ns147="http://ws.apache.org/ns/synapse"/>
    </class>
    <log level="custom">
        <property
            expression="get-property('REST_NTLM_AUTH_RESPONSE_CODE')"
            name="HTTP_RESPONSE_CODE" xmlns:ns="http://org.apache.synapse/xsd"/>
    </log>
    <respond/>
</sequence>
```

### Potential Issues

  - Given the class mediator only implements a very basic REST client, it may be necessary to further enhance/customise the existing implementation to suit your needs. For example, if an API requires particular headers, you'll need to modify the class to accept a list of headers from a new property. It may be possible to set these properties via Axis2 properties, and this should be explored, but has not been tested at the time of writing.

  - Having not spent time working with the response payload, it could be that rendering the response as text within the SOAP body could be difficult to work with. In this case, the mediator class should be modified to correctly parse the response body string into the desired data type (i.e. JSON or XML).
